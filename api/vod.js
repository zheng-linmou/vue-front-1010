import request from '@/utils/request'
export default {
    // 发送修改密码验证码
  getPlayAuth(vid) {
    return request({
      url: `/eduvod/video/getPlayAuth/${vid}`,
      method: 'get',
    })
  },
  // 判断有没有资格观看
  baofang(courseId,sourceId,memberId) {
    return request({
      url: `/eduvod/video/bofang/${courseId}/${sourceId}/${memberId}`,
      method: 'get',
    })
  },
 
}