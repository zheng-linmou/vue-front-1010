import request from '@/utils/request'
export default {
    // 发送修改密码验证码
  updatePassword(formItem) {
    return request({
      url: `/educenter/ucenter-member/updatepassword`,
      method: 'post',
      data: formItem
    })
  },
  // 根据手机号发送验证码
  sendCode(phone) {
    return request({
      url: `/edumsm/msm/updatePassWord/${phone}`,
      method: 'get'
    })
  },
}