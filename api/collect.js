import request from '@/utils/request'
export default {

  // 1.查询所有收藏
  getAllCollect(page,limit) {
    return request({
      url: `/eduservice/edu-course-collect/getAllCollect/${page}/${limit}`,
      method: 'get'
    })
  },
  // 2.收藏该课程
  collectCourse(courseId){
    return request({
      url: `/eduservice/edu-course-collect/collectCourse/${courseId}`,
      method: 'get'
    })
  },
  // 3.取消收藏该课程
  cancelCollectCourse(courseId){
    return request({
      url: `/eduservice/edu-course-collect/cancelCollectCourse/${courseId}`,
      method: 'delete'
    })
  },
  // 4.查询是否收藏
  hasCollect(courseId){
    return request({
      url: `/eduservice/edu-course-collect/hasCollect/${courseId}`,
      method: 'get'
    })
  }
}