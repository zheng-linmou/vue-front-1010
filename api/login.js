import request from '@/utils/request'
export default {

  // 登录
  loginMember(memberInfo) {
    return request({
      url: `/educenter/ucenter-member/login`,
      method: 'post',
      data: memberInfo
    })
  },

  // 根据token获取用户信息的方法
  // 不用传参request
  getLogininUserInfo(){
    return request({
      url: `/educenter/ucenter-member/getMemberInfo`,
      method: 'get'
    })
  }

  
  
}