import request from '@/utils/request'
export default {

    // 分页课程查询方法
  getCourseList(page,limit,searchObj) {
    return request({
      url: `/eduservice/coursefront/getFrontCourseList/${page}/${limit}`,
      method: 'post',
      data: searchObj
    })
  },

  // 查询所有课程分类的方法(一二级分类)
  getAllSubject(){
    return request({
        url: `/eduservice/edu-subject/getAllSubject`,
        method: 'get',
      })
  },
  
  // 查询课程详情方法
  getAllCourseInfo(courseId){
    return request({
        url: `/eduservice/coursefront/getFrontCourseInfo/${courseId}`,
        method: 'get',
      })
  },

  
 

}