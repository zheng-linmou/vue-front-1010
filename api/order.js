import request from '@/utils/request'
export default {

  // 生成订单的接口
  createOrders(courseId) {
    return request({
      url: `/eduorder/order/createOrder/${courseId}`,
      method: 'post',
    })
  },

  //获取订单信息
  getOrderInfo(id){
    return request({
      url: `/eduorder/order/getOrderId/${id}`,
      method: 'get'
    })
  },

  // 生成二维码的方法
  createNative(orderNo) {
    return request({
      url: `/eduorder/pay-log/createNative/${orderNo}`,
      method: 'get',
    })
  },

  // 查询订单状态的二维码
  queryPayStatus(orderNo) {
    return request({
      url: `/eduorder/pay-log/queryPayStatus/${orderNo}`,
      method: 'get',
    })
  },

  // 查询自己的全部订单
  getOrdersByMemberId(page,limit){
    return request({
      url: `/eduorder/order/getOrdersByMemberId/${page}/${limit}`,
      method: 'get',
    })
  },
  
  // 删除订单
  removeOrder(id){
    return request({
      url: `/eduorder/order/removeOrder/${id}`,
      method: 'delete',
    })
  }

  
  
}