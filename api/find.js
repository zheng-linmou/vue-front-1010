import request from '@/utils/request'
export default {

  // 根据手机号发送验证码
  sendCode(phone) {
    return request({
      url: `/edumsm/msm/sendPassWord/${phone}`,
      method: 'get'
    })
  },

  // 找回密码
  findPassword(formItem) {
    return request({
      url: `/educenter/ucenter-member/findpassword`,
      method: 'post',
      data: formItem
    })
  },
}